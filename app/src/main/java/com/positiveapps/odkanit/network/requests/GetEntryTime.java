package com.positiveapps.odkanit.network.requests;

import java.util.HashMap;

/**
 * Created by Niv on 4/4/2016.
 */
public class GetEntryTime {
    private int userId;

    public GetEntryTime(int userId) {
        this.userId = userId;
    }



    public HashMap<String,Object> bodyMap() {
        HashMap<String,Object> result = new HashMap<String,Object>();
        result.put("userID",userId);

        return result;
    }
}
