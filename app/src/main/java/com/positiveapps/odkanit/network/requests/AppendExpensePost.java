package com.positiveapps.odkanit.network.requests;

import com.google.gson.Gson;
import com.positiveapps.odkanit.main.MyApp;
import com.positiveapps.odkanit.util.DateUtil;

import java.util.HashMap;

import javax.xml.datatype.XMLGregorianCalendar;

import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

/**
 * Created by Niv on 3/10/2016.
 */
public class AppendExpensePost {
    private int UserID;
    private String TikNumber;
    private String Date;
    private double Amount;
    private String Description;
    private String Reference;
    private String ImageBase64;

    public AppendExpensePost(int userID, String tikNumber, String date, double amount, String description, String reference, String imageBase64) {
        UserID = userID;
        TikNumber = tikNumber;
        Date = date;
        Amount = amount;
        Description = description;
        Reference = reference;
        ImageBase64 = imageBase64;
    }

    public HashMap<String,Object> bodyMap() {
        HashMap<String,Object> result = new HashMap<String,Object>();
        result.put("UserID",UserID);
        result.put("TikNumber",TikNumber);
        result.put("Date",Date);
        result.put("Amount", Amount);
        result.put("Description",Description);
        result.put("Reference",Reference);
        if (!ImageBase64.equals(""))
        result.put("ImageBase64",ImageBase64);
        return result;
    }

//    public String body(){
//        AppendExpensePost aep= new AppendExpensePost(getUserID(), getTikNumber(),
//                getDate(),
//                getAmount(), getDescription(), getReference(),
//                getImageBase64());
//        Gson gson = new Gson();
//        String json = gson.toJson(aep);
//        return json;
//    }
//    public String body2(){
//        AppendExpensePost aep= new AppendExpensePost(getUserID(), getTikNumber(),
//                getDate(),
//                getAmount(), getDescription(), getReference(),
//                getImageBase64());
//        Gson gson = new Gson();
//        String json = gson.toJson(aep);
//        AppendExpensePost jsonExpense=new AppendExpensePost(getUserID(), getTikNumber(),
//                getDate(),
//                getAmount(), getDescription(), getReference(),
//                getImageBase64());
//
//
//        TypedInput in = new TypedByteArray("application/json", json.getBytes("UTF-8"));
//        FooResponse response = foo.postRawJson(in);
//
//        return json;
//    }

    public int getUserID() {
        return UserID;
    }

    public String getTikNumber() {
        return TikNumber;
    }

    public String getDate() {
        return Date;
    }

    public double getAmount() {
        return Amount;
    }

    public String getDescription() {
        return Description;
    }

    public String getReference() {
        return Reference;
    }

    public String getImageBase64() {
        return ImageBase64;
    }


    /*
    {
	"Amount":1.26743233E+15,
	"Date":"\/Date(928142400000+0300)\/",
	"Description":"String content",
	"ImageBase64":"String content",
	"Reference":"String content",
	"TikNumber":"String content",
	"UserID":5
}
     */
}
