package com.positiveapps.odkanit.network.response;
public class ActionResult {

    private String ErrorMessage;
    private boolean Success;

    public String getErrorMessage() {
        return ErrorMessage;
    }

    public void setErrorMessage(String ErrorMessage) {
        this.ErrorMessage = ErrorMessage;
    }
    public boolean getSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }


        @Override
        public String toString() {
        	return "ActionResult{" +
			"ErrorMessage=" + ErrorMessage + ";" + 
			"Success=" + Success + ";" + 
			'}';
	}
}
