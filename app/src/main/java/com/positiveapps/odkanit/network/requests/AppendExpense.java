package com.positiveapps.odkanit.network.requests;


import java.util.HashMap;

import javax.xml.datatype.XMLGregorianCalendar;

/***************************************---- Request Class ------********************/

public class AppendExpense {


    private int userID;
    private String tikNumber;
    private XMLGregorianCalendar date;
    private double amount;
    private String description;
    private String reference;
    private String imageBase64;

    public AppendExpense() {
    }

    public AppendExpense(int userID,String tikNumber,XMLGregorianCalendar date,double amount,String description,String reference,String imageBase64) {
        this.userID = userID;
        this.tikNumber = tikNumber;
        this.date = date;
        this.amount = amount;
        this.description = description;
        this.reference = reference;
        this.imageBase64 = imageBase64;
    }




    public HashMap<String,Object> bodyMap() {
        HashMap<String,Object> result = new HashMap<String,Object>();
        result.put("userID",userID);
        result.put("tikNumber",tikNumber);
        result.put("date",date);
        result.put("amount",amount);
        result.put("description",description);
        result.put("reference",reference);
        result.put("imageBase64",imageBase64);
        return result;
    }
}
