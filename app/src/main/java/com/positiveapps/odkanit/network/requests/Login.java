package com.positiveapps.odkanit.network.requests;


import java.util.HashMap;

/***************************************---- Request Class ------********************/

public class Login {

    private String officeCode;
    private String userName;
    private String password;
    private String deviceType;
    private String deviceUDID;

    public Login() {
    }

    public Login(String officeCode,String userName,String password,String deviceType,String deviceUDID) {
        this.officeCode = officeCode;
        this.userName = userName;
        this.password = password;
        this.deviceType = deviceType;
        this.deviceUDID = deviceUDID;
    }




    public HashMap<String,Object> bodyMap() {
        HashMap<String,Object> result = new HashMap<String,Object>();
        result.put("officeCode",officeCode);
        result.put("userName",userName);
        result.put("password",password);
        result.put("deviceType",deviceType);
        result.put("UDID",deviceUDID);
        return result;
    }
}
