package com.positiveapps.odkanit.network.response;

import java.util.ArrayList;

public class PhonebookItem {

    private String Address;
    private String EMail;
    private String Name;
    private ArrayList<PhoneNumberItem> Phones;
    private String Type;

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }
    public String getEMail() {
        return EMail;
    }

    public void setEMail(String EMail) {
        this.EMail = EMail;
    }
    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }
    public ArrayList<PhoneNumberItem> getPhones() {
        return Phones;
    }

    public void setPhones(ArrayList<PhoneNumberItem> Phones) {
        this.Phones = Phones;
    }
    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }


        @Override
        public String toString() {
        	return "PhonebookItem{" +
			"Address=" + Address + ";" + 
			"EMail=" + EMail + ";" + 
			"Name=" + Name + ";" + 
			"Phones=" + Phones + ";" + 
			"Type=" + Type + ";" + 
			'}';
	}
}
