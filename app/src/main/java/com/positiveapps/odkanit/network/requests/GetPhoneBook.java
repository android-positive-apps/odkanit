package com.positiveapps.odkanit.network.requests;


import java.util.HashMap;

/***************************************---- Request Class ------********************/

public class GetPhoneBook {

    private int userID;
    private String searchString;

    public GetPhoneBook() {
    }

    public GetPhoneBook(int userID,String searchString) {
        this.userID = userID;
        this.searchString = searchString;
    }




    public HashMap<String,Object> bodyMap() {
        HashMap<String,Object> result = new HashMap<String,Object>();
        result.put("userID",userID);
        result.put("searchString",searchString);
        return result;
    }
}
