package com.positiveapps.odkanit.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.positiveapps.odkanit.main.MyApp;
import com.positiveapps.odkanit.network.requests.BaseRequestObject;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;





/**
 * Created by natiapplications on 16/08/15.
 */
public class NetworkManager {

    public static final String BASE_URL = "http://appsrv.od.co.il/mobile/OdcanitMobileApp.MobileDataService.svc/web";




    private Context context;
    private RestAdapter restAdapter;
    private ApiInterface apiInterface;
    // connectivity manager to check network state before send the requests
    private ConnectivityManager connectivityManager;

    private NetworkManager(Context context) {
        this.context = context;
    }

    private static NetworkManager instance;

    public static NetworkManager getInstance (Context context) {
        if (instance == null){
            instance = new NetworkManager(context);
        }
        instance.initRestAdapter();
        return instance;
    }

    public ApiInterface initRestAdapter (){

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(16, TimeUnit.SECONDS); // connect timeout
        client.setReadTimeout(16, TimeUnit.SECONDS);
        OkClient okClient = new OkClient(client);



        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setClient(okClient)
                .build();

        apiInterface = restAdapter.create(ApiInterface.class);

        return  apiInterface;

    }

    public ApiInterface initRestAdapterBasic (){

       /* OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(15, TimeUnit.SECONDS); // connect timeout
        client.setReadTimeout(15, TimeUnit.SECONDS);
        OkClient okClient = new OkClient(client);*/



        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .setLogLevel(RestAdapter.LogLevel.BASIC)
                .build();

        apiInterface = restAdapter.create(ApiInterface.class);

        return  apiInterface;

    }



    public void makeRequest (BaseRequestObject request,NetworkCallback<?> callback){
        if (request != null){
            request.request(this.apiInterface,callback);
        }
    }







    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) MyApp.appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            return false;
        }

    }

    public static boolean isWifiAvailable () {
        ConnectivityManager connManager = (ConnectivityManager)MyApp.appContext.
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (mWifi.isConnected()) {
            return true;
        }
        return false;
    }









}
