package com.positiveapps.odkanit.network.requests;


import java.util.HashMap;

/***************************************---- Request Class ------********************/

public class TiksList {

    private int userID;
    private String clientNumber;
    private String searchString;

    public TiksList() {
    }

    public TiksList(int userID,String clientNumber,String searchString) {
        this.userID = userID;
        this.clientNumber = clientNumber;
        this.searchString = searchString;
    }




    public HashMap<String,Object> bodyMap() {
        HashMap<String,Object> result = new HashMap<String,Object>();
        result.put("userID",userID);
        result.put("clientNumber",clientNumber);
        result.put("searchString",searchString);
        return result;
    }
}
