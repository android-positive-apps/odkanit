package com.positiveapps.odkanit.database.tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.positiveapps.odkanit.database.BaseContentProvider;
import com.positiveapps.odkanit.database.ContentProvaiderDataSourseAdapter;
import com.positiveapps.odkanit.models.Customer;
import com.positiveapps.odkanit.database.providers.CustomerContentProvider;

/**
 * Created by NIVO on 13/12/2015.
 */
public class TableCustomer extends ContentProvaiderDataSourseAdapter<Customer> {

    // table name
    public static final String TABLE_CUSTOMER = "customer";

    // columns names
    public final static String TAG_ID           = "_id";
    public final static String TAG_NAME         = "name";
    public final static String TAG_LASTNAME     = "lastname";
    public final static String TAG_PHONE        = "phone";
    public final static String TAG_PHONE1 = "phone1";
    public final static String TAG_PHONE2 = "phone2";
    public final static String TAG_FAX          = "fax";
    public final static String TAG_EMAIL        = "email";
    public final static String TAG_ADDRESS      = "address";
    public final static String TAG_IS_COMPANY   = "iscompany";

    public static final String[] ALL_COLUMNS = {
         TAG_ID,TAG_NAME,TAG_LASTNAME,TAG_PHONE, TAG_PHONE1,TAG_PHONE2,TAG_FAX,TAG_EMAIL,TAG_ADDRESS,TAG_IS_COMPANY

    };

    public static final int COLUMN_SIZE = ALL_COLUMNS.length;

    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table " + TABLE_CUSTOMER
            + "("
            + TAG_ID               + " integer primary key autoincrement, "
            + TAG_NAME            + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + TAG_LASTNAME      + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + TAG_PHONE             + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + TAG_PHONE1 + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + TAG_PHONE2 + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + TAG_EMAIL             + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + TAG_ADDRESS             + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + TAG_IS_COMPANY             + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + TAG_FAX            + " text not null DEFAULT " + DEFAULT_TEXT_VALUE
            +");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOMER);
        onCreate(database);
    }


    public static TableCustomer instance;



    public static TableCustomer getInstance (Context context){
        if (instance == null){
            instance = new TableCustomer(context);
        }
        return instance;
    }


    private TableCustomer(Context context) {
        super(context);

    }


    @Override
    protected Uri getProviderUri() {
        return CustomerContentProvider.CONTENT_URI;
    }

    @Override
    public void close() {
        if (databasehelper != null){
            databasehelper.close();
        }

        instance = null;
    }



    @Override
    protected void onCreateDataSourse(BaseContentProvider contentProvider) {
        super.onCreateDataSourse(contentProvider);

    }


    @Override
    public Customer cursorToEntity(Cursor cursor) {



        long id=  cursor.getLong(cursor.getColumnIndexOrThrow(TableCustomer            .TAG_ID          ));
        String name=cursor.getString(cursor.getColumnIndexOrThrow(TableCustomer.TAG_NAME));
        String lastName=cursor.getString(cursor.getColumnIndexOrThrow(TableCustomer        .TAG_LASTNAME    ));
        String phone=cursor.getString(cursor.getColumnIndexOrThrow(TableCustomer .TAG_PHONE       ));
        String Phone1=cursor.getString(cursor.getColumnIndexOrThrow(TableCustomer     .TAG_PHONE1));
        String Phone2=cursor.getString(cursor.getColumnIndexOrThrow(TableCustomer     .TAG_PHONE2));
        String fax=cursor.getString(cursor.getColumnIndexOrThrow(TableCustomer       .TAG_FAX         ));
        String email=cursor.getString(cursor.getColumnIndexOrThrow(TableCustomer       .TAG_EMAIL       ));
        String address=cursor.getString(cursor.getColumnIndexOrThrow(TableCustomer       .TAG_ADDRESS     ));
        String iscompany=cursor.getString(cursor.getColumnIndexOrThrow(TableCustomer.TAG_IS_COMPANY));

        Customer Customer = new Customer(id,name,lastName,iscompany,phone,Phone1,Phone2,fax,email,address);


        return Customer;
    }

    @Override
    public ContentValues entityToContentValue(Customer entity) {
        ContentValues values = new ContentValues();

        values.put(TableCustomer.TAG_NAME, entity.getName());
        values.put(TableCustomer.TAG_LASTNAME, entity.getLastName());
        values.put(TableCustomer.TAG_PHONE, entity.getPhone());
        values.put(TableCustomer.TAG_PHONE1, entity.getPhone1());
        values.put(TableCustomer.TAG_PHONE2, entity.getPhone2());
        values.put(TableCustomer.TAG_FAX, entity.getFax());
        values.put(TableCustomer.TAG_ADDRESS, entity.getAddress());
        values.put(TableCustomer.TAG_EMAIL, entity.getEmail());
        values.put(TableCustomer.TAG_IS_COMPANY, entity.getType());

        return values;
    }



    @Override
    protected BaseContentProvider createContentProvider() {
        return new CustomerContentProvider();
    }


    @Override
    public synchronized Customer insert(Customer entity) {
        return super.insert(entity);

    }

    public Customer isEntryAllradyExsit (Customer toCheck){

        try {
            Customer msg = readEntityByID(toCheck.getId());
            if (msg != null){
                return msg;
            }
        } catch (Exception e) {
            return null;
        }


        return null;

    }

    public Customer insertOrUpdate (Customer toAdd) {
        Customer check = isEntryAllradyExsit(toAdd);
        if (check != null){
            return update(check.getId(), toAdd);
        }else{
            return insert(toAdd);
        }
    }
}

