package com.positiveapps.odkanit.database.providers;

import android.database.sqlite.SQLiteOpenHelper;

import com.positiveapps.odkanit.database.BaseContentProvider;
import com.positiveapps.odkanit.database.DatabaseHelper;
import com.positiveapps.odkanit.database.tables.Table10Contacts;



public class LastContactsContentProvider extends BaseContentProvider {

    @Override
    protected String provideAuthority() {
        // TODO Auto-generated method stub
        return "com.positiveapps.odkanit.database.providers.LastContactsContentProvider";
    }


    @Override
    protected String provideBasePath() {
        // TODO Auto-generated method stub
        return "lastcontacts";
    }


    @Override
    protected String provideTableName() {
        // TODO Auto-generated method stub
        return Table10Contacts.TABLE_CUSTOMER;
    }


    @Override
    protected String provideColumnID() {
        // TODO Auto-generated method stub
        return Table10Contacts.TAG_ID;
    }


    @Override
    protected String[] provideAllColumns() {
        // TODO Auto-generated method stub
        return Table10Contacts.ALL_COLUMNS;
    }


    @Override
    protected SQLiteOpenHelper provideSQLiteHalper() {
        // TODO Auto-generated method stub
        return new DatabaseHelper(getContext());
    }


}

