package com.positiveapps.odkanit.database.providers;

import android.database.sqlite.SQLiteOpenHelper;

import com.positiveapps.odkanit.database.BaseContentProvider;
import com.positiveapps.odkanit.database.DatabaseHelper;
import com.positiveapps.odkanit.database.tables.TableClient;
import com.positiveapps.odkanit.database.tables.TableTik;

/**
 * Created by NIVO on 30/12/2015.
 */
public class TikContentProvider  extends BaseContentProvider {

    @Override
    protected String provideAuthority() {
        // TODO Auto-generated method stub
        return "com.positiveapps.odkanit.database.providers.TikContentProvider";
    }


    @Override
    protected String provideBasePath() {
        // TODO Auto-generated method stub
        return "tik";
    }


    @Override
    protected String provideTableName() {
        // TODO Auto-generated method stub
        return TableTik.TABLE_TIK;
    }


    @Override
    protected String provideColumnID() {
        // TODO Auto-generated method stub
        return TableTik.TAG_ID;
    }


    @Override
    protected String[] provideAllColumns() {
        // TODO Auto-generated method stub
        return TableTik.ALL_COLUMNS;
    }


    @Override
    protected SQLiteOpenHelper provideSQLiteHalper() {
        // TODO Auto-generated method stub
        return new DatabaseHelper(getContext());
    }


}
