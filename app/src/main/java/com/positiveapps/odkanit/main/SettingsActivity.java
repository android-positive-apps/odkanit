package com.positiveapps.odkanit.main;

import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;

import com.positiveapps.positiveapps.odcanit.R;


/**
 * Created by NIVO on 29/12/2015.
 */
public class SettingsActivity extends PreferenceActivity {


    private String name;
    private String officeName;
    private EditTextPreference prefUsername;
    private EditTextPreference prefUserOffice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.prefs);

        prefUsername   = (EditTextPreference)findPreference("prefUsername");
        prefUserOffice = (EditTextPreference)findPreference("prefUserOffice");





        prefUsername.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                name= prefUsername.getText();
                prefUsername.setSummary(name);
                officeName= prefUserOffice.getText();

                MyApp.userProfile.setFirstName(name);


                return true;
            }
        });


    }




}
