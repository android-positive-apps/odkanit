package com.positiveapps.odkanit.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.positiveapps.odkanit.main.MyApp;
import com.positiveapps.odkanit.models.Tik;
import com.positiveapps.positiveapps.odcanit.R;


public class BagCursorAdapter extends CursorAdapter {
    //create holder
    class ViewHolder {
        //insert attributes to holder
        long id;
        public TextView textView;
        public ImageView imageView;
    }


    public BagCursorAdapter(Context context, Cursor c) {
        super(context, c);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.customer_list_item, parent, false);
        ViewHolder holder = new ViewHolder();
        holder.textView = (TextView)view.findViewById(R.id.textViewCustomer_item);

        holder.imageView=(ImageView)view.findViewById(R.id.imageViewCustomerList);
        view.setTag(holder);



        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        Tik tik = MyApp.tableTik.cursorToEntity(cursor);
        ViewHolder holder = (ViewHolder) view.getTag();

        String name = tik.getName()+"("+tik.getNumber()+")";

        holder.textView.setText(name);
//        holder.textView.setTag(String.valueOf(tik.getNumber()));






    }
}
