package com.positiveapps.odkanit.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by NIVO on 30/12/2015.
 */


public class ClientOBJ {
    long id;

    @SerializedName("Name")
    String Name;
    @SerializedName("Number")
    String number;

    long timeInMil=0;




    public ClientOBJ(long id, String name, String number) {
        this.id = id;
        this.Name = name;
        this.number = number;
    }

    public ClientOBJ(String name, String number) {
        this.Name = name;
        this.number = number;
    }

    public ClientOBJ(long id, String name, String number, long timeInMil) {
        this.id = id;
        this.Name = name;
        this.number = number;
        this.timeInMil = timeInMil;
    }

    public ClientOBJ(String name, String number, long timeInMil) {
        Name = name;
        this.number = number;
        this.timeInMil = timeInMil;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public long getTimeInMil() {
        return timeInMil;
    }

    public void setTimeInMil(long timeInMil) {
        this.timeInMil = timeInMil;
    }
}
