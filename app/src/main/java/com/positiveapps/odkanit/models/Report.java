package com.positiveapps.odkanit.models;

import com.positiveapps.odkanit.util.DateUtil;

import org.joda.time.DateTime;

/**
 * Created by Niv on 3/3/2016.
 */
public class Report implements  Comparable<Report> {
    private String reportDate;
    private String reportBag;
    private String reportTime;

    public Report(String reportDate, String reportBag, String reportTime) {
        this.reportDate = reportDate;
        this.reportBag = reportBag;
        this.reportTime = reportTime;
    }

    @Override
    public int compareTo(Report report) {
        DateTime thisDate = new DateTime(DateUtil.getMillisOfParsingDateString(DateUtil.FORMAT_JUST_DATE, getReportDate()));
        DateTime nextReportDate = new DateTime(DateUtil.getMillisOfParsingDateString(DateUtil.FORMAT_JUST_DATE, report.getReportDate()));

        if (getReportDate() == null || report.getReportDate() == null)
            return 0;
        return nextReportDate.compareTo(thisDate);
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public String getReportBag() {
        return reportBag;
    }

    public void setReportBag(String reportBag) {
        this.reportBag = reportBag;
    }

    public String getReportTime() {
        return reportTime;
    }

    public void setReportTime(String reportTime) {
        this.reportTime = reportTime;
    }




}
