package com.positiveapps.odkanit.models;

/**
 * Created by NIVO on 30/12/2015.
 */
public class AppendExpense {


    long id;
    String tiknumber;
    String date;
    double amount;
    String description;
    String reference;
    String imageBase64;


    public AppendExpense(long id, String tiknumber, String date, double amount, String description, String reference, String imageBase64) {
        this.id = id;
        this.tiknumber = tiknumber;
        this.date = date;
        this.amount = amount;
        this.description = description;
        this.reference = reference;
        this.imageBase64 = imageBase64;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTiknumber() {
        return tiknumber;
    }

    public void setTiknumber(String tiknumber) {
        this.tiknumber = tiknumber;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getImageBase64() {
        return imageBase64;
    }

    public void setImageBase64(String imageBase64) {
        this.imageBase64 = imageBase64;
    }
}
