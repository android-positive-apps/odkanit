package com.positiveapps.odkanit.models;

/**
 * Created by NIVO on 24/12/2015.
 */
public class Customer {

    private Long id;
    private String Name = "";
    private String LastName = "";
    private String Type = "";
    private String Phone = "";
    private String Phone1 = "";
    private String Phone2 = "";
    private String Fax = "";
    private String Email = "";
    private String Address = "";
    private long TimeInMil= 0;

    public Customer() {
    }

    public Customer(Long id, String name, String lastName, String type, String phone, String phone1, String phone2, String fax, String email, String address, long timeInMil) {
        this.id = id;
        Name = name;
        LastName = lastName;
        Type = type;
        Phone = phone;
        Phone1 = phone1;
        Phone2 = phone2;
        Fax = fax;
        Email = email;
        Address = address;
        TimeInMil = timeInMil;
    }

    public Customer(String name, String lastName, String type, String phone, String phone1, String phone2, String fax, String email, String address, long timeInMil) {
        Name = name;
        LastName = lastName;
        Type = type;
        Phone = phone;
        Phone1 = phone1;
        Phone2 = phone2;
        Fax = fax;
        Email = email;
        Address = address;
        TimeInMil = timeInMil;
    }

    public Customer(String name, String lastName, String type, String phone, String phone1, String phone2, String fax, String email, String address) {
        Name = name;
        LastName = lastName;
        Type = type;
        Phone = phone;
        Phone1 = phone1;
        Phone2 = phone2;
        Fax = fax;
        Email = email;
        Address = address;
    }

    public Customer(Long id, String name, String lastName, String type, String phone, String phone1, String phone2, String fax, String email, String address) {
        this.id = id;
        Name = name;
        LastName = lastName;
        Type = type;
        Phone = phone;
        Phone1 = phone1;
        Phone2 = phone2;
        Fax = fax;
        Email = email;
        Address = address;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getPhone1() {
        return Phone1;
    }

    public void setPhone1(String phone1) {
        Phone1 = phone1;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String fax) {
        Fax = fax;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        this.Type = type;
    }

    public long getTimeInMil() {
        return TimeInMil;
    }

    public void setTimeInMil(long timeInMil) {
        TimeInMil = timeInMil;
    }

    public String getPhone2() {
        return Phone2;
    }

    public void setPhone2(String phone2) {
        Phone2 = phone2;
    }
}
