package com.positiveapps.odkanit.models;

/**
 * Created by NIVO on 27/12/2015.
 */
public class ExpensesObject {
    private Long id;
    private String Date;
    private String customer;
    private String bag;
    private String time;
    private String des;
    private String image;
    private String Esmahta;
    private double Latitude;
    private double Longitude;

    public ExpensesObject(Long id, String date, String customer, String bag, String time, String des, String image, String esmahta, double latitude, double longitude) {
        this.id = id;
        Date = date;
        this.customer = customer;
        this.bag = bag;
        this.time = time;
        this.des = des;
        this.image = image;
        Esmahta = esmahta;
        Latitude = latitude;
        Longitude = longitude;
    }

    public ExpensesObject(String date, String customer, String bag, String time, String des, String image, String esmahta, double latitude, double longitude) {
        Date = date;
        this.customer = customer;
        this.bag = bag;
        this.time = time;
        this.des = des;
        this.image = image;
        Esmahta = esmahta;
        Latitude = latitude;
        Longitude = longitude;
    }

    public ExpensesObject(String date, String customer, String bag, String time, String des, String image, String esmahta) {
        Date = date;
        this.customer = customer;
        this.bag = bag;
        this.time = time;
        this.des = des;
        this.image = image;
        Esmahta = esmahta;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getBag() {
        return bag;
    }

    public void setBag(String bag) {
        this.bag = bag;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEsmahta() {
        return Esmahta;
    }

    public void setEsmahta(String esmahta) {
        Esmahta = esmahta;
    }

    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double latitude) {
        Latitude = latitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }
}
