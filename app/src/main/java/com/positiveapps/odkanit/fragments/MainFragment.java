package com.positiveapps.odkanit.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.positiveapps.odkanit.main.ContactsActivity;
import com.positiveapps.odkanit.main.ExpensesActivity;
import com.positiveapps.odkanit.main.MainActivity;
import com.positiveapps.odkanit.main.MyApp;
import com.positiveapps.odkanit.main.PresenceReportingActivity;
import com.positiveapps.odkanit.main.SummaryActivity;
import com.positiveapps.odkanit.network.requests.GetEntryTime;
import com.positiveapps.odkanit.network.requests.Login;
import com.positiveapps.odkanit.network.response.LoginResult;
import com.positiveapps.odkanit.objects.MainButtons;
import com.positiveapps.odkanit.objects.TextBox;
import com.positiveapps.odkanit.util.DateUtil;
import com.positiveapps.odkanit.util.DeviceUtil;
import com.positiveapps.odkanit.util.FragmentsUtil;
import com.positiveapps.odkanit.util.ToastUtil;
import com.positiveapps.positiveapps.odcanit.R;

import org.joda.time.DateTime;
import org.joda.time.DateTimeFieldType;
import org.joda.time.format.DateTimeFormatter;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import roboguice.inject.InjectView;

/**
 * Created by NIVO on 21/12/2015.
 */
public class MainFragment extends BaseFragment implements View.OnClickListener {

    private TextBox tb;
    @InjectView(R.id.helloText)
    TextView helloText;
    @InjectView(R.id.helloTextDes)
    TextView helloTextDes;
    @InjectView(R.id.rishumButton)
    LinearLayout rishumButton;
    @InjectView(R.id.dateText)
    TextView dateText;
    @InjectView(R.id.hourText)
    TextView hourText;

    @InjectView(R.id.timerLinear)
    LinearLayout timerLinear;

    private MainFragmentListener listener;
    private MainButtons mainButtons1;
    private String helloTextAppend;

    public MainFragment() {
    }


    public interface MainFragmentListener {

        void onCLick();

    }

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        return fragment;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // try to set the activity as the listener:
        try {
            listener = (MainFragmentListener) activity;
        } catch (ClassCastException e) {
            // the activity does not implement FragmentListListener!
            throw new ClassCastException(activity.toString()
                    + " must implement FragmentListListener!");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);


    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);





        helloTextAppend = getString(R.string.hello_des);
        String helloTextString2 =  helloTextAppend +" "+ MyApp.generalSettings.getStartDayTime();
        helloTextDes.setText(helloTextString2);



        //todo get from app prefress the name

        rishumButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openWriteChargesActivity();


            }
        });


        mainButtons1 = (MainButtons) view.findViewById(R.id.MainButtons1);

        mainButtons1.sefer_telefonim_button().setOnClickListener(this);
        mainButtons1.buttonDoh_mesakem().setOnClickListener(this);
        mainButtons1.nohahut_button().setOnClickListener(this);
        mainButtons1.hozaot_button().setOnClickListener(this);


    }

    @Override
    public void onResume() {
        super.onResume();




        String name= MyApp.appPreference.getUserProfil().getFirstName();
        String helloTextString =  getString(R.string.shalom)+name;
        helloText.setText(helloTextString);

        getBaseActivity().setupActionBar(R.id.app_actionbar, getString(R.string.odkanit_name));
        getBaseActivity().getActionBarTextView().setTextColor(Color.WHITE);


//        String isSameDay= MyApp.generalSettings.getIssameday();
//
//        if (isSameDay.equals(DateUtil.getCurrentDateAsString(DateUtil.FORMAT_JUST_DATE))) {
//            String helloTextString2 = helloTextAppend + " " + MyApp.generalSettings.getStartDayTime();
//            helloTextDes.setText(helloTextString2);
//        }else {
//            helloTextDes.setText(R.string.didnt_start_day);
//        }

//        DateTime dateTime = new DateTime( System.currentTimeMillis());
        System.out.println("hour:"+DateUtil.getTimeAsStringByMilli(DateUtil.FORMAT_JUST_TIME, System.currentTimeMillis()));
//        String time = (dateTime.getHourOfDay()<9? "0"+ dateTime.getHourOfDay():dateTime.getHourOfDay()+"")
//                +":"+
//                (dateTime.getMinuteOfHour()<9? "0"+ dateTime.getMinuteOfHour():dateTime.getMinuteOfHour()+"");
//                ;
        String time = DateUtil.getTimeAsStringByMilli(DateUtil.FORMAT_JUST_TIME, System.currentTimeMillis());
        String date= DateUtil.getCurrentDateAsString(DateUtil.FORMAT_JUST_DATE);

        dateText.setText(date);
        hourText.setText(time);

        if (MyApp.userSettings.getTimerState() == 1){
            timerLinear.setVisibility(View.VISIBLE);
        }else {
            timerLinear.setVisibility(View.GONE);

        }

        getEntryTime();

    }

    private void getEntryTime() {
        MyApp.networkManager.initRestAdapter().getEntryTime(new GetEntryTime(MyApp.userProfile.getUserId()).bodyMap(), new Callback<String>() {
            @Override
            public void success(String s, Response response) {
                if (response!=null) {
                    if (!s.equals("")) {
                        MyApp.generalSettings.setIsdaystarted(true);
                        DateTime dt = new DateTime(s);
//                        String time= DateUtil.getDateAsStringByMilli(DateUtil.FORMAT_DEAFULT,dt.getMillis());
//                        String time= DateUtil.getDateAsStringByMilli(DateUtil.FORMAT_DEAFULT,dt.getMillis());
                        String time = DateUtil.getTimeAsStringByMilli(DateUtil.FORMAT_JUST_TIME, dt.getMillis())+" "+(dt.getDayOfMonth()<10 ? "0"+dt.getDayOfMonth():dt.getDayOfMonth())+"/"+(dt.getMonthOfYear()<10?"0"+dt.getMonthOfYear():dt.getMonthOfYear())+"/"+dt.getYear() /*DateUtil.getCurrentDateAsString(DateUtil.FORMAT_JUST_DATE)*/;

//                        int day = dt.getDayOfMonth();
//                        int month = dt.getMonthOfYear();
//                        int year = dt.getYear();
//                        int h = dt.getHourOfDay();
//                        int m = dt.getMinuteOfHour();
//                        String time = day + "/" + month + "/" + year + " " + h + ":" + m;
                        MyApp.generalSettings.setIssameday(DateUtil.getCurrentDateAsString(DateUtil.FORMAT_JUST_DATE));
                        MyApp.generalSettings.setStartDayTime(time);
                        MyApp.generalSettings.setIsdaystarted(true);
                        String helloTextString2 = helloTextAppend + " " + time;
                        helloTextDes.setText(helloTextString2);

                    } else {
                        MyApp.generalSettings.setIsdaystarted(false);
                        helloTextDes.setText(R.string.didnt_start_day);
                    }
                }else {
                    MyApp.generalSettings.setIsdaystarted(false);
                    helloTextDes.setText(R.string.didnt_start_day);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                ToastUtil.toaster(getActivity().getResources().getString(R.string.start_day_time_error),true);
            }
        });



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sefer_telefonim_button:
                Intent contactsIntent = new Intent(getActivity(), ContactsActivity.class);
                startActivity(contactsIntent);




                break;
            case R.id.buttonDoh_mesakem:
                Intent sumIntent= new Intent(getActivity(), SummaryActivity.class);
                startActivity(sumIntent);



                break;
            case R.id.hozaot_button:
                Intent i = new Intent(getActivity(), ExpensesActivity.class);
                startActivity(i);


                break;
            case R.id.nohahut_button:
                Intent nohehutIntent = new Intent(getActivity(), PresenceReportingActivity.class);
                startActivity(nohehutIntent);

                break;
        }
    }

    private void openWriteChargesActivity() {


        FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
                WriteChargesFragment.newInstance(),
                R.id.main_container);

    }

}
