package com.positiveapps.odkanit.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.positiveapps.odkanit.main.MyApp;
import com.positiveapps.odkanit.models.Customer;
import com.positiveapps.odkanit.objects.TextBox;
import com.positiveapps.odkanit.util.ContentProviderUtil;
import com.positiveapps.odkanit.util.Helper;
import com.positiveapps.positiveapps.odcanit.R;

/**
 * Created by NIVO on 03/01/2016.
 */
public class ContactsDetailsFragment extends BaseFragment {
    private TextBox TextBoxTelephone;
    private TextBox TextBoxOfficephone;
    private TextBox TextBoxFax;
    private TextBox TextBoxMail;
    private TextView addressText;
    private LinearLayout addressLayout;
    private Menu menuItem;
    private TextBox TextBoxOfficephone2;

    public ContactsDetailsFragment() {
    }


    public static ContactsDetailsFragment newInstance(long id,boolean fromSearch) {
        ContactsDetailsFragment fragment = new ContactsDetailsFragment();
        Bundle args = new Bundle();
        args.putLong("id", id);
        args.putBoolean("fromSearch", fromSearch);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_customer_details, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        setHasOptionsMenu(true);



        findViews(view);
        Bundle args = getArguments();
        long id = args.getLong("id", 0);
        boolean fromSearch= args.getBoolean("fromSearch", false);
        Customer c;
        if (fromSearch) {
             c = MyApp.tableCustomer.readEntityByID(id);
        }else {
             c = MyApp.table10Contacts.readEntityByID(id);

        }

        setDetails(c);

        setListeners(c);




    }


    private void setListeners( Customer c) {
        TextBoxTelephone.getMainEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openDialactivity(TextBoxTelephone.getMainText());
            }
        });
        TextBoxOfficephone.getMainEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialactivity(TextBoxOfficephone.getMainText());

            }
        });
        TextBoxOfficephone2.getMainEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialactivity(TextBoxOfficephone2.getMainText());

            }
        });
        TextBoxMail.getMainEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentProviderUtil.openMailApp(getActivity(), TextBoxMail.getMainText(), "", "");

            }
        });

        addressLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });


    }

    private void showDialog(){

        Uri uri = Uri.parse("geo:0,0?q=" + addressText.getText().toString().replace(" ", "+"));

        Helper.showMap(uri,getActivity());

//
//        LayoutInflater factory = LayoutInflater.from(getActivity());
//        final View deleteDialogView = factory.inflate(
//                R.layout.choose_waze_dialog, null);
//        final AlertDialog deleteDialog = new AlertDialog.Builder(getActivity()).create();
//        deleteDialog.setView(deleteDialogView);
//        deleteDialogView.findViewById(R.id.buttonWaze).setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//             ContentProviderUtil.openWaze(getActivity(), addressText.getText().toString());
//
//
//                //your business logic
//                deleteDialog.dismiss();
//            }
//        });
//        deleteDialogView.findViewById(R.id.buttonCancel).setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                deleteDialog.dismiss();
//
//            }
//        });
//
//        deleteDialog.show();


    }

    private void openDialactivity(String mainText) {

        Helper.openDialer(getActivity(), mainText);
    }



    private void setDetails(Customer c) {

        getBaseActivity().setupActionBar(R.id.app_actionbar, c.getName() + "");


        if(c.getAddress().equals("")){
            addressLayout.setVisibility(View.GONE);
        }else {
            addressText.setText(c.getAddress());
        }

        if(c.getEmail().equals("")){
            TextBoxMail.setVisibility(View.GONE);
        }else {
            TextBoxMail.setMainText(c.getEmail());
        }

        if(c.getPhone().equals("")){
            TextBoxTelephone.setVisibility(View.GONE);
        }else {
            TextBoxTelephone.setMainText(c.getPhone());
        }
        if(c.getFax().equals("")){
//            TextBoxFax.getBasicFrame().setVisibility(View.GONE);
            TextBoxFax.setVisibility(View.GONE);
        }else {
            TextBoxFax.setMainText(c.getFax());
        }
        if(c.getPhone2().equals("")){
//            TextBoxFax.getBasicFrame().setVisibility(View.GONE);
            TextBoxOfficephone2.setVisibility(View.GONE);
        }else {
            TextBoxOfficephone2.setMainText(c.getPhone2());
        }
        if(c.getPhone1().equals("")){
            TextBoxOfficephone.setVisibility(View.GONE);
        }else {
            TextBoxOfficephone.setMainText(c.getPhone1());
        }

    }

    private void findViews(View view) {
        TextBoxTelephone = (TextBox) view.findViewById(R.id.TextBoxTelephone);
        TextBoxOfficephone = (TextBox) view.findViewById(R.id.TextBoxCellphone);
        TextBoxOfficephone2 = (TextBox) view.findViewById(R.id.TextBoxCellphone2);
        TextBoxFax = (TextBox) view.findViewById(R.id.TextBoxFax);
        TextBoxMail = (TextBox) view.findViewById(R.id.TextBoxMail);
        addressText=(TextView)view.findViewById(R.id.addressText);
        addressLayout=(LinearLayout)view.findViewById(R.id.addressLayout);



    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.menu_home, menu);
        MenuItem item = menu.findItem(R.id.action_search);
//        MenuItem item2 = menu.findItem(R.id.action_home);
//        item2.setVisible(false);
        item.setVisible(false);

        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        System.out.println("onOptionsItemSelected");
        switch (item.getItemId()) {
            case R.id.action_home2:
                System.out.println("onOptionsItemSelected action_home2");

                getActivity().finish();
                return true;


        }
        return false;
    }
}
