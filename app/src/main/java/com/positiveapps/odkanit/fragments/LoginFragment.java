package com.positiveapps.odkanit.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.positiveapps.odkanit.main.MainActivity;
import com.positiveapps.odkanit.main.MyApp;
import com.positiveapps.odkanit.network.requests.Login;
import com.positiveapps.odkanit.network.response.LoginResult;
import com.positiveapps.odkanit.objects.TextBox;
import com.positiveapps.odkanit.util.DeviceUtil;
import com.positiveapps.odkanit.util.TextUtil;
import com.positiveapps.odkanit.util.ToastUtil;
import com.positiveapps.positiveapps.odcanit.R;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import roboguice.inject.InjectView;

/**
 * Created by NIVO on 21/12/2015.
 */
public class LoginFragment extends BaseFragment implements View.OnClickListener {


    @InjectView(R.id.loginButton)
    private ImageButton loginButton;
    private TextBox textBoxCustomerCode;
    private TextBox textBoxCustomerName;
    private TextBox textBoxCustomerPass;



    public LoginFragment(){

    }
    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);

        // android:supportsRtl="true"

    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loginButton.setOnClickListener(this);
        textBoxCustomerCode= (TextBox)view.findViewById(R.id.textBoxCustomerCode);
        textBoxCustomerName= (TextBox)view.findViewById(R.id.textBoxCustomerName);
        textBoxCustomerPass= (TextBox)view.findViewById(R.id.textBoxCustomerPass);




    }



    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.loginButton:


                if (TextUtil.baseFieldsValidation(textBoxCustomerCode.getEditText(),
                        textBoxCustomerName.getEditText()/*,textBoxCustomerPass.getEditText()*/)){
                    String userName= textBoxCustomerName.getMainText();
                    String userCode= textBoxCustomerCode.getMainText();
                    String userPass= textBoxCustomerPass.getMainText();

                    performLogin(userName,userPass,userCode);
                }

                break;

        }
    }


    private void performLogin ( final String userName, final String pass,  final String userCode){
        showProgressDialog();

        MyApp.networkManager.initRestAdapter().login(new Login(userCode, userName,
                        pass, "android", DeviceUtil.getDeviceUDID()).bodyMap(),
                new Callback<LoginResult>() {

                    @Override
                    public void success(LoginResult login, Response response) {
                        dismisProgressDialog();
                        //then continue
                        if (login.getApproved()) {
                            MyApp.userProfile.setFirstName(login.getDisplayName());
                            MyApp.userProfile.setUserId(login.getUserID());
                            MyApp.appPreference.getUserProfil().setLoggedIn(true);
                            MyApp.userProfile.setLOGED_name(userName);
                            MyApp.userProfile.setLoggedOfficeCode(userCode);
                            MyApp.userProfile.setLOGED_pass(pass);
                            Intent loginIntent = new Intent(getActivity(), MainActivity.class);
                            getActivity().startActivity(loginIntent);
                            getActivity().finish();
                        }else {
                            ToastUtil.toaster(login.getErrorMessage(),true);
//                            System.out.println("--->F"+login.getErrorMessage());
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        dismisProgressDialog();
                        ToastUtil.toaster(getResources().getString(R.string.login_server_error), true);

                    }
                });


}
}
