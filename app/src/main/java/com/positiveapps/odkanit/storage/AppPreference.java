/**
 *
 */
package com.positiveapps.odkanit.storage;


import android.content.Context;
import android.content.SharedPreferences;

import com.positiveapps.odkanit.main.MyApp;
import com.positiveapps.odkanit.util.DateUtil;


/**
 * @author Nati Gabay
 */
public class AppPreference {


    public static final int UNITS_TYPE_KM = 1;
    public static final int UNITS_TYPE_MILES = 2;
    public static final int SKI_PASS_TYPE_AREA = 1;
    public static final int SKI_PASS_TYPE_RESORT = 2;


    private static AppPreference instance;
    private static Context context = MyApp.appContext;

    private static PreferenceUserProfile userProfil;
    private static PreferenceUserSettings userSettings;
    private static PreferenceGeneralSettings generalSettings;

    private AppPreference(Context context) {
        userSettings = PreferenceUserSettings.getUserSettingsInstance();
        userProfil = PreferenceUserProfile.getUserProfileInstance();
        generalSettings = PreferenceGeneralSettings.getGeneralSettingsInstance();
    }

    public static AppPreference getInstans(Context context) {
        if (instance == null) {
            instance = new AppPreference(context);
        }
        setInstancContext(context);
        return instance;
    }

    public static void removeInstance() {
        userSettings.removeInstance();
        userProfil.removeInstance();
        generalSettings.removeInstance();
        instance = null;
    }

    private static void setInstancContext(Context context) {
        AppPreference.context = context;
    }


    /**
     * @return the userProfil
     */
    public PreferenceUserProfile getUserProfil() {
        return userProfil;
    }

    /**
     * @param userProfil the userProfil to set
     */
    public void setUserProfil(PreferenceUserProfile userProfil) {
        this.userProfil = userProfil;
    }

    /**
     * @return the userSettings
     */
    public PreferenceUserSettings getUserSettings() {
        return userSettings;
    }

    /**
     * @param userSettings the userSettings to set
     */
    public void setUserSettings(PreferenceUserSettings userSettings) {
        this.userSettings = userSettings;
    }

    /**
     * @return the generalSettings
     */
    public PreferenceGeneralSettings getGeneralSettings() {
        return generalSettings;
    }

    /**
     * @param generalSettings the generalSettings to set
     */
    public void setGeneralSettings(PreferenceUserSettings generalSettings) {
        this.userSettings = generalSettings;
    }


    public static class PreferenceGeneralSettings {

        public final String GENERAL_SETTINGS = "GeneralSettings";

        public static final String LAT = "Lat";
        public static final String LON = "Lon";
        public static final String GCM_KEY = MyApp.appVersion + "GcmKey";
        public static final String SCREEN_WIDTH = "ScreenWidth";
        public static final String SCREEN_HIGHT = "ScreenHight";
        public static final String LAST_READ_MESSAGE_COUNT = "LastReadMessageCount";
        public static final String USER_LOGIN_LEVEL = "userLoginLevel";
        /* refine feed */
        public static final String PICUP_RADIUS = "picupRadius";
        public static final String DELIVERY_TIME = "deliveryTime";
        public static final String DELIVERY_PRICE = "deliveryPrice";
        public static final String DIMENSION = "dimension";
        public static final String PROPERTIES_FRAGILE = "propertiesFragile";
        public static final String PROPERTIES_WEIGHT = "propertiesWeight";
        public static final String PROPERTIES_TEMPERTURE = "propertiesTemperture";

        private static final String ADMIN_PHONE = "adminPhone";
        private static final String ADMIN_MAIL = "adminMail";
        private static final String HAS_GENERATE_DB = "has_generate_db12";
        private static final String APPLICATION_NAME = "ApplicationName";
        private static final String VERSION = "Version";
        private static final String SERVER_TIME = "ServerTime";
        private static final String LAST_FAQ_UPDATED = "LastFAQUpdate";
        private static final String PACKAGE_EXPIRATION = "PackageExpiration";
        private static final String DELIVERIES_REFESH_RATE = "DeliveriesRefreshRate";
        private static final String MEETING_TIME = "MeetingTime";
        private static final String START_DAY_TIME = "StartDayTime";
        public static final String ISSAMEDAY = "issameday";
        public static final String IS_DAY_STARTED = "isdaystarted";
        public static final String TEMP_DATE = "tempDate";
        public static final String TEMP_CLIENT = "client";
        public static final String TEMP_CLIENT_NAME = "tempClientName";

        public static final String DATE_WCF = "datewcf";
        public static final String CLIENT_WCF = "clientWcf";
        public static final String CLIENT_NUMBER_WCF = "clientNumWcf";
        public static final String BAG_WCF = "bagWCF";
        public static final String HOURS_WCF = "hoursWCF";
        public static final String DES_WCF = "desWCF";
        public static final String BAG_NUMBER_WCF = "BAG_NUMBER_WCF";
        public static final String MANUAL = "MANUAL";

        public static final String BAG_NUMBER_EX = "BAG_NUMBER_EX";
        public static final String DATE_EX = "dateEX";
        public static final String CLIENT_EX = "clientEX";
        public static final String BAG_EX = "bagEX";
        public static final String DES_EX = "desEX";
        public static final String PAY_EX = "payEX";
        public static final String ESMACHTA_EX = "ESMACHTAEX";


        public static final String IS_EXPENSE_SAVED = "isExpenseSaved";






        private static final String UNVIEW_NEGO_COUNT = "UnViewNegoCount";

        private SharedPreferences generalSettings;
        public static PreferenceGeneralSettings generalSettingInstance;

        private PreferenceGeneralSettings() {
            generalSettings = context.getSharedPreferences(GENERAL_SETTINGS,
                    Context.MODE_PRIVATE);
        }

        public static PreferenceGeneralSettings getGeneralSettingsInstance() {
            if (generalSettingInstance == null) {
                generalSettingInstance = new PreferenceGeneralSettings();
            }
            return generalSettingInstance;
        }

        public void removeInstance() {
            generalSettings.edit().clear().commit();
            generalSettingInstance = null;
        }

        public void setApplicationName(String name) {
            generalSettings.edit().putString(APPLICATION_NAME, name).commit();
        }

        public String getApplicationName() {
            return generalSettings.getString(APPLICATION_NAME, "PikPack");
        }

        public void setAppServerVersion(String version) {
            generalSettings.edit().putString(VERSION, version).commit();
        }

        public String getAppServerVersion() {
            return generalSettings.getString(VERSION, "1.0.0");
        }


        public void setServerTime(String time) {
            generalSettings.edit().putString(SERVER_TIME, time).commit();
        }

        public String getServerTime() {
            return generalSettings.getString(SERVER_TIME, DateUtil.getCurrentDateAsString
                    (DateUtil.FORMAT_AMERICAN_SERVER));
        }


        public void setLasFaqUpdated(long set) {
            generalSettings.edit().putLong(LAST_FAQ_UPDATED, set).commit();
        }

        public long getLastFaqUpdated() {
            return generalSettings.getLong(LAST_FAQ_UPDATED, DateUtil.getCurrentDateInMilli());
        }


        public void setPackageExprationCount(long count) {
            generalSettings.edit().putLong(PACKAGE_EXPIRATION, count).commit();
        }

        public void setUserLoginLevel(int set) {
            generalSettings.edit().putInt(USER_LOGIN_LEVEL, set).commit();
        }

        public long getPackageExprationCount() {
            return /*generalSettings.getLong(PACKAGE_EXPIRATION,*/3 * (60 * 60 * 1000);
        }


        public void setDeliveriesRefreshRate(boolean set) {
            generalSettings.edit().putBoolean(DELIVERIES_REFESH_RATE, set).commit();
        }

        public boolean isDeliveriesNeedRefreshRate() {
            return generalSettings.getBoolean(DELIVERIES_REFESH_RATE, false);
        }

        public void setPicupRadius(String picupRadius) {
            generalSettings.edit().putString(PICUP_RADIUS, picupRadius).commit();
        }

        public String getPicupRadius() {
            return generalSettings.getString(PICUP_RADIUS, "");
        }

        public void setDeliveryTime(String deliveryTime) {
            generalSettings.edit().putString(DELIVERY_TIME, deliveryTime).commit();
        }

        public String getDeliveryTime() {
            return generalSettings.getString(DELIVERY_TIME, "");
        }

        public void setDeliveryPrice(String deliveryPrice) {
            generalSettings.edit().putString(DELIVERY_PRICE, deliveryPrice).commit();
        }

        public String getDeliveryPrice() {
            return generalSettings.getString(DELIVERY_PRICE, "");
        }

        public void setDimension(String dimension) {
            generalSettings.edit().putString(DIMENSION, dimension).commit();
        }

        public String getDimension() {
            return generalSettings.getString(DIMENSION, "");
        }

        public void setPropertiesFragile(String propertiesFragile) {
            generalSettings.edit().putString(PROPERTIES_FRAGILE, propertiesFragile).commit();
        }

        public String getPropertiesFragile() {
            return generalSettings.getString(PROPERTIES_FRAGILE, "");
        }

        public void setPropertiesWeight(String propertiesWeight) {
            generalSettings.edit().putString(PROPERTIES_WEIGHT, propertiesWeight).commit();
        }

        public String getPropertiesWeight() {
            return generalSettings.getString(PROPERTIES_WEIGHT, "");
        }

        public void setPropertiesTemperture(String propertiesTemperture) {
            generalSettings.edit().putString(PROPERTIES_TEMPERTURE, propertiesTemperture).commit();
        }

        public String Temperture() {
            return generalSettings.getString(PROPERTIES_TEMPERTURE, "");
        }


        public void setLat(String toSet) {
            generalSettings.edit().putString(LAT, toSet).commit();
        }

        public void setLon(String toSet) {
            generalSettings.edit().putString(LON, toSet).commit();
        }

        public void setGCMKey(String gcmKey) {
            generalSettings.edit().putString(GCM_KEY, gcmKey).commit();
        }


        public void setScreenWidth(int width) {
            generalSettings.edit().putInt(SCREEN_WIDTH, width).commit();
        }

        public void setScreenHight(int hight) {
            generalSettings.edit().putInt(SCREEN_HIGHT, hight).commit();
        }

        public void setLastReadMessageCount(String deliveryID, int count) {
            generalSettings.edit().putInt(deliveryID + LAST_READ_MESSAGE_COUNT, count).commit();
        }

        public String getLat() {
            return generalSettings.getString(LAT, "0.0");
        }

        public String getLon() {
            return generalSettings.getString(LON, "0.0");
        }


        public String getLocation() {
            return getLat() + "," + getLon();
        }

        public String getGCMKey() {
            return generalSettings.getString(GCM_KEY, "");
        }

        public int getUserLoginLevel() {
            return generalSettings.getInt(USER_LOGIN_LEVEL, 0);
        }

        public int getScreenWidth() {
            return generalSettings.getInt(SCREEN_WIDTH, 0);
        }

        public int getScreenHight() {
            return generalSettings.getInt(SCREEN_HIGHT, 0);
        }

        public int getLastReadMessageCount(String deliveryID) {
            return generalSettings.getInt(deliveryID + LAST_READ_MESSAGE_COUNT, 0);
        }

        public String getAdminPhone() {
            return generalSettings.getString(ADMIN_PHONE, "");
        }

        public void setAdminPhone(String adminPhone) {
            generalSettings.edit().putString(ADMIN_PHONE, adminPhone).commit();
        }

        public String getAdminMail() {
            return generalSettings.getString(ADMIN_MAIL, "");
        }

        public void setAdminMail(String adminMail) {
            generalSettings.edit().putString(ADMIN_MAIL, adminMail).commit();
        }


        public void setHasGenerateDB(boolean set) {
            generalSettings.edit().putBoolean(HAS_GENERATE_DB, set).commit();
        }

        public boolean isHasGenerateDB() {
            return generalSettings.getBoolean(HAS_GENERATE_DB, false);
        }


        public void setUnviewNegoValue(int set) {
            generalSettings.edit().putInt(UNVIEW_NEGO_COUNT, set).commit();
        }

        public int getUnviewNegoValue() {
            return generalSettings.getInt(UNVIEW_NEGO_COUNT, 0);
        }

        public String getMeetingTime(){
            return generalSettings.getString(MEETING_TIME, "0");
        }
        public void setMeetingTime(String set){
            generalSettings.edit().putString(MEETING_TIME,set).commit();
        }

        public void setStartDayTime(String time){
            generalSettings.edit().putString(START_DAY_TIME, time).commit();
        }
        public String getStartDayTime(){
            return generalSettings.getString(START_DAY_TIME, "0");
        }

        public void setIsdaystarted(boolean isdaystarted) {
            generalSettings.edit().putBoolean(IS_DAY_STARTED, isdaystarted).commit();
        }

        public boolean getIsdaystarted(){
            return generalSettings.getBoolean(IS_DAY_STARTED, false);
        }

        public void setIssameday(String issameday) {
            generalSettings.edit().putString(ISSAMEDAY, issameday).commit();
        }

        public String getIssameday(){
            return generalSettings.getString(ISSAMEDAY, "");
        }

        public void setTempDate(long tempDate){

            generalSettings.edit().putLong(TEMP_DATE, tempDate).commit();
        }
        public long getTempDate(){
            return generalSettings.getLong(TEMP_DATE, System.currentTimeMillis());
        }
        public void setTempClient(String tempClient){
            generalSettings.edit().putString(TEMP_CLIENT, tempClient).commit();
        }
        public String getTempClient(){
            return generalSettings.getString(TEMP_CLIENT, "");
        }

        public void setDateWcf(long DateWcf){
            generalSettings.edit().putLong(DATE_WCF, DateWcf).commit();
        }
        public long getDateWcf(){
            return generalSettings.getLong(DATE_WCF, 0);
        }

        public void setClientWcf(String tempClient){
            generalSettings.edit().putString(CLIENT_WCF, tempClient).commit();
        }
        public String getClientWcf(){
            return generalSettings.getString(CLIENT_WCF, "");
        }


        public String getClientNumber(){
            return generalSettings.getString(CLIENT_NUMBER_WCF, "");
        }
        public void setClientNumber(String clientNumber){
            generalSettings.edit().putString(CLIENT_NUMBER_WCF, clientNumber).commit();

        }


        public void setBagWcf(String BagWcf){
            generalSettings.edit().putString(BAG_WCF, BagWcf).commit();
        }
        public String getBagWcf(){
            return generalSettings.getString(BAG_WCF, "");
        }
        public void setHoursWcf(long hoursWCF){
            generalSettings.edit().putLong(HOURS_WCF, hoursWCF).commit();
        }
        public long getHoursWcf(){
            return generalSettings.getLong(HOURS_WCF, 0);
        }
        public void setDesWcf(String desWCF){
            generalSettings.edit().putString(DES_WCF, desWCF).commit();
        }
        public String getDesWcf(){
            return generalSettings.getString(DES_WCF, "");
        }
        ///////////////////////////////
        public void setDateEx(long dateEX){
            generalSettings.edit().putLong(DATE_EX, dateEX).commit();
        }
        public long getDateEx(){
            return generalSettings.getLong(DATE_EX, 0);
        }

        public void setClientEx(String clientEX){
            generalSettings.edit().putString(CLIENT_EX, clientEX).commit();
        }
        public String getClientEx(){
            return generalSettings.getString(CLIENT_EX, "");
        }
        public void setBagEx(String bagEX){
            generalSettings.edit().putString(BAG_EX, bagEX).commit();
        }
        public String getBagEx(){
            return generalSettings.getString(BAG_EX, "");
        }

        public void setDesEx(String DesEx){
            generalSettings.edit().putString(DES_EX, DesEx).commit();
        }
        public String getDesEx(){
            return generalSettings.getString(DES_EX, "");
        }

        public void setPayEx(String PayEx){
            generalSettings.edit().putString(PAY_EX, PayEx).commit();
        }
        public String getPayEx(){
            return generalSettings.getString(PAY_EX, "");
        }
        public void setEsmachtaEx(String EsmachtaEx){
            generalSettings.edit().putString(ESMACHTA_EX, EsmachtaEx).commit();
        }
        public String getEsmachtaEx(){
            return generalSettings.getString(ESMACHTA_EX, "");
        }

        public void setBagNumberWcf(String BagNumberWcf){
            generalSettings.edit().putString(BAG_NUMBER_WCF, BagNumberWcf).commit();
        }
        public String getBagNumberWcf(){
            return generalSettings.getString(BAG_NUMBER_WCF, "");
        }
        public void setBagNumberEX(String BagNumberEX){
            generalSettings.edit().putString(BAG_NUMBER_EX, BagNumberEX).commit();
        }
        public String getBagNumberEX(){
            return generalSettings.getString(BAG_NUMBER_EX, "");
        }

        public void setManual(boolean Manual){
            generalSettings.edit().putBoolean(MANUAL, Manual).commit();
        }
        public boolean getManual(){
            return generalSettings.getBoolean(MANUAL, true);
        }


        public void setIsExpenseSaved(boolean isExpenseSaved){
            generalSettings.edit().putBoolean(IS_EXPENSE_SAVED, isExpenseSaved).commit();
        }
        public boolean getIsExpenseSaved(){
            return generalSettings.getBoolean(IS_EXPENSE_SAVED,true);
        }

/////////////////////////

        public void setTempClientName(String tempClientName){
    generalSettings.edit().putString(TEMP_CLIENT_NAME, tempClientName).commit();
}
        public String getTempClientName(){
            return generalSettings.getString(TEMP_CLIENT_NAME, "");
        }

    }


    public static class PreferenceUserProfile {

        public static final String USER_PROFILE = "UserProfile";
        public static final String LOGED_IN = "LogedIn";
        public static final String LOGED_OFFICECODE = "LogedOfficeCode";
        public static final String LOGED_name = "LogedName";
        public static final String LOGED_pass = "LogedPass";


        private static final String USER_TYPE = "userType";
        private static final String USER_FIRST_NAME = "firstNaem";
        private static final String USER_LAST_NAME = "lastName";
        private static final String USER_EMAIL = "userEmail";
        private static final String USER_FBID = "userFBID";
        private static final String USER_TWITTER_ID = "twitterID";
        private static final String USER_LOCAL_IMAGE_PATH = "userLocalImagePath";
        private static final String USER_SERVER_IMAGE_URL = "userServerImageUrl";
        private static final String OFFICIAL_DOCUMENT1 = "officialDocument1";
        private static final String OFFICIAL_DOCUMENT2 = "officialDocument2";
        private static final String OFFICIAL_DOCUMENT3 = "officialDocument3";
        private static final String PROOF_OF_RESIDENCE = "proofOfResidence";
        private static final String STREET = "street";
        private static final String CITY = "city";
        private static final String ZIP_CODE = "zipCode";
        private static final String COURIER_MAIN_VEHICLE = "courierMainVheicle";
        private static final String DATE_OF_BIRTH = "dateOfBirth";
        private static final String MOBILE_PHONE_NUMBER = "mobilePhoneNumber";
        private static final String COUNTRY = "country";
        private static final String STREET_AND_NUMBER_BUILDING = "streeAndNumberBuilding";
        private static final String HAV_GOT_THE_GIFT = "hav_got_the_gift";
        private static final String USER_ID = "appUser_id";
        private static final String SECRET = "AppUserSecret";
        private static final String FACEBOOK_USER_NAME = "facebook_user_name";
        private static final String TWITTER_USER_NAME = "twitter_user_name";
        private static final String FACEBOOK_TOKEN = "facebook_token";
        private static final String TWITTER_TOKEN = "twitter_token";
        private static final String TWITTER_SECRET = "twitter_secret";
        private static final String CREATED = "created_at";
        private static final String EMAIL_VERIFIED = "email_verified";
        private static final String SMS_AUTH = "sms_auth";
        private static final String PROOF_OF_RES = "proof_of_res";
        private static final String IS_COURIER = "is_courier";
        private static final String CORIER_TOTAL_REVIEWS = "courier_total_reviews";
        private static final String CORIER_AVERAGE = "courier_average";
        private static final String CORIER_AVERAGE_SUM = "courier_average_sum";
        private static final String OWNER_TOTAL_REVIEWS = "owner_total_reviews";
        private static final String OWNER_AVERAGE = "owner_average";
        private static final String OWNER_AVERAGE_SUM = "owner_average_sum";

        private SharedPreferences userProfile;
        private static PreferenceUserProfile userProfileInstance;

        private PreferenceUserProfile() {
            userProfile = context.getSharedPreferences(USER_PROFILE, Context.MODE_PRIVATE);
        }

        public static PreferenceUserProfile getUserProfileInstance() {
            if (userProfileInstance == null) {
                userProfileInstance = new PreferenceUserProfile();
            }
            return userProfileInstance;
        }

        public void removeInstance() {
            userProfile.edit().clear().commit();
            userProfileInstance = null;
        }


        public void setCreatedAt(String set) {
            userProfile.edit().putString(CREATED, set).commit();
        }

        public String getCreatedAT() {
            return userProfile.getString(CREATED, "");
        }

        public void setProofOfRes(String set) {
            userProfile.edit().putString(PROOF_OF_RES, set).commit();
        }

        public String getProofOfRes() {
            return userProfile.getString(PROOF_OF_RES, "");
        }


        public void setCourierTotalReviews(int set) {
            userProfile.edit().putInt(CORIER_TOTAL_REVIEWS, set).commit();
        }

        public int getCourierTotalReviews() {
            return userProfile.getInt(CORIER_TOTAL_REVIEWS, 0);
        }

        public void setCourierAverage(int set) {
            userProfile.edit().putInt(CORIER_AVERAGE, set).commit();
        }

        public int getCourierAverage() {
            return userProfile.getInt(CORIER_AVERAGE, 0);
        }

        public void setCourierAverageSum(int set) {
            userProfile.edit().putInt(CORIER_AVERAGE_SUM, set).commit();
        }

        public int getCourierAverageSum() {
            return userProfile.getInt(CORIER_AVERAGE_SUM, 0);
        }


        public void setOwnerTotalReviews(int set) {
            userProfile.edit().putInt(OWNER_TOTAL_REVIEWS, set).commit();
        }

        public int getOwnerTotalReviews() {
            return userProfile.getInt(OWNER_TOTAL_REVIEWS, 0);
        }

        public void setOwnerAverage(int set) {
            userProfile.edit().putInt(OWNER_AVERAGE, set).commit();
        }

        public int getOwnerAverage() {
            return userProfile.getInt(OWNER_AVERAGE, 0);
        }

        public void setOwnerAverageSum(int set) {
            userProfile.edit().putInt(OWNER_AVERAGE_SUM, set).commit();
        }

        public int getOwnerAverageSum() {
            return userProfile.getInt(OWNER_AVERAGE_SUM, 0);
        }


        public void setEmeailVerified(boolean set) {
            userProfile.edit().putBoolean(EMAIL_VERIFIED, set).commit();
        }

        public boolean isEmailVerified() {
            return userProfile.getBoolean(EMAIL_VERIFIED, false);
        }


        public void setSMSAuth(boolean set) {
            userProfile.edit().putBoolean(SMS_AUTH, set).commit();
        }

        public boolean isSMSAuth() {
            return userProfile.getBoolean(SMS_AUTH, false);
        }

        public void setIsCourier(boolean set) {
            userProfile.edit().putBoolean(IS_COURIER, set).commit();
        }

        public boolean isCourier() {
            return userProfile.getBoolean(IS_COURIER, false);
        }

        public void setDateOfBirth(String dateOfBirth) {
            userProfile.edit().putString(DATE_OF_BIRTH, dateOfBirth).commit();
        }

        public String getDateOfBirth() {
            return userProfile.getString(DATE_OF_BIRTH, "");
        }

        public void setMobilePhoneNumber(String mobilePhoneNumber) {
            userProfile.edit().putString(MOBILE_PHONE_NUMBER, mobilePhoneNumber).commit();
        }

        public String getMobilePhoneNumber() {
            return userProfile.getString(MOBILE_PHONE_NUMBER, "");
        }

        public void setCountry(String country) {
            userProfile.edit().putString(COUNTRY, country).commit();
        }

        public String getCountry() {
            return userProfile.getString(COUNTRY, "");
        }

        public void setStreeAndNumberBuilding(String streeAndNumberBuilding) {
            userProfile.edit().putString(STREET_AND_NUMBER_BUILDING, streeAndNumberBuilding).commit();
        }

        public String getStreeAndNumberBuilding() {
            return userProfile.getString(STREET_AND_NUMBER_BUILDING, "");
        }

        public void setHavGotTheGift(boolean set) {
            userProfile.edit().putBoolean(HAV_GOT_THE_GIFT, set).commit();
        }

        public void setStreet(String street) {
            userProfile.edit().putString(STREET, street).commit();
        }

        public String getStreet() {
            return userProfile.getString(STREET, "");
        }

        public void setCity(String city) {
            userProfile.edit().putString(CITY, city).commit();
        }

        public String getCity() {
            return userProfile.getString(CITY, "");
        }

        public void setZipCode(String zipCode) {
            userProfile.edit().putString(ZIP_CODE, zipCode).commit();
        }

        public String getZipCode() {
            return userProfile.getString(ZIP_CODE, "");
        }


        public void setFirstName(String set) {
            userProfile.edit().putString(USER_FIRST_NAME, set).commit();
        }

        public String getFirstName() {
            return userProfile.getString(USER_FIRST_NAME, "");
        }

        public void setLastName(String set) {
            userProfile.edit().putString(USER_LAST_NAME, set).commit();
        }

        public String getLastName() {
            return userProfile.getString(USER_LAST_NAME, "");
        }

        public void setUserLocalImagePath(String imagePath) {
            userProfile.edit().putString(USER_LOCAL_IMAGE_PATH, imagePath).commit();
        }

        public String getUserLocalImagePath() {
            return userProfile.getString(USER_LOCAL_IMAGE_PATH, "");
        }

        public void setOfficialDocumentImagePath1(String imagePath) {
            userProfile.edit().putString(OFFICIAL_DOCUMENT1, imagePath).commit();
        }

        public String getOfficialDocumentImagePath1() {
            return userProfile.getString(OFFICIAL_DOCUMENT1, "");
        }

        public void setOfficialDocumentImagePath2(String imagePath) {
            userProfile.edit().putString(OFFICIAL_DOCUMENT2, imagePath).commit();
        }

        public String getOfficialDocumentImagePath2() {
            return userProfile.getString(OFFICIAL_DOCUMENT2, "");
        }

        public void setOfficialDocumentImagePath3(String imagePath) {
            userProfile.edit().putString(OFFICIAL_DOCUMENT3, imagePath).commit();
        }

        public String getOfficialDocumentImagePath3() {
            return userProfile.getString(OFFICIAL_DOCUMENT3, "");
        }

        public void setProofOfResidenceImagePath(String imagePath) {
            userProfile.edit().putString(PROOF_OF_RESIDENCE, imagePath).commit();
        }

        public String getproofOfResidenceImagePath() {
            return userProfile.getString(PROOF_OF_RESIDENCE, "");
        }

        public void setCourierMainVehicle(String imagePath) {
            userProfile.edit().putString(COURIER_MAIN_VEHICLE, imagePath).commit();
        }

        public String getCourierMainVehicle() {
            return userProfile.getString(COURIER_MAIN_VEHICLE, "");
        }


        public void setUserServerImageUrl(String url) {
            userProfile.edit().putString(USER_SERVER_IMAGE_URL, url).commit();
        }

        public String getUserServerImageURl() {
            return userProfile.getString(USER_SERVER_IMAGE_URL, "");
        }

        public void setUserId(int userId) {
            userProfile.edit().putInt(USER_ID, userId).commit();
        }

        public void setUserSecret(String secret) {
            userProfile.edit().putString(SECRET, secret).commit();
        }

        public int getUserId() {
            return userProfile.getInt(USER_ID, 0);
        }

        public String getUserSecret() {
            return userProfile.getString(SECRET, "");
        }


        public String getUserName() {
            return getFirstName() + " " + getLastName();
        }

        public boolean isHavGotTheGift() {
            return userProfile.getBoolean(HAV_GOT_THE_GIFT, false);
        }

        public void setUserFBID(String set) {
            userProfile.edit().putString(USER_FBID, set).commit();
        }

        public String getUserFBID() {
            return userProfile.getString(USER_FBID, "");
        }


        public void setUserTwitterID(String set) {
            userProfile.edit().putString(USER_TWITTER_ID, set).commit();
        }

        public String getUserTwitterID() {
            return userProfile.getString(USER_TWITTER_ID, "");
        }


        public void setUserType(int type) {
            userProfile.edit().putInt(USER_TYPE, type).commit();
        }

        public int getUserType() {
            return userProfile.getInt(USER_TYPE, 0);
        }


        public void setUserEmail(String email) {
            userProfile.edit().putString(USER_EMAIL, email).commit();
        }

        public String getUserEmail() {
            return userProfile.getString(USER_EMAIL, "");
        }

        public void setFacebookUserName(String set) {
            userProfile.edit().putString(FACEBOOK_USER_NAME, set).commit();
        }

        public String getFacebookUserName() {
            return userProfile.getString(FACEBOOK_USER_NAME, "");
        }


        public void setTwitterUserName(String set) {
            userProfile.edit().putString(TWITTER_USER_NAME, set).commit();
        }

        public String getTwitterUserName() {
            return userProfile.getString(TWITTER_USER_NAME, "");
        }


        public void setTwitterToken(String set) {
            userProfile.edit().putString(TWITTER_TOKEN, set).commit();
        }

        public String getTwitterToken() {
            return userProfile.getString(TWITTER_TOKEN, "");
        }

        public void setFacebookToken(String set) {
            userProfile.edit().putString(FACEBOOK_TOKEN, set).commit();
        }

        public String getFacebookToken() {
            return userProfile.getString(FACEBOOK_TOKEN, "");
        }


        public void setTwitterSecret(String set) {
            userProfile.edit().putString(TWITTER_SECRET, set).commit();
        }

        public String getTwitterSecret() {
            return userProfile.getString(TWITTER_SECRET, "");
        }


        public void removeAll() {
            userProfile.edit().clear().commit();
        }

        public Boolean getLoggedIn(){
            return userProfile.getBoolean(LOGED_IN, false);
        }
        public void setLoggedIn(boolean b){

            userProfile.edit().putBoolean(LOGED_IN, b).commit();
        }
//////////////////
public String getLoggedOfficeCode(){
    return userProfile.getString(LOGED_OFFICECODE, null);
}
        public void setLoggedOfficeCode(String loggedOfficeCode){

            userProfile.edit().putString(LOGED_OFFICECODE, loggedOfficeCode).commit();
        }
        /////////
        public String getLOGED_name(){
            return userProfile.getString(LOGED_name, null);
        }
        public void setLOGED_name(String loged_name){

            userProfile.edit().putString(LOGED_name, loged_name).commit();
        }
        ///////
        public String getLOGED_pass(){
            return userProfile.getString(LOGED_pass, null);
        }
        public void setLOGED_pass(String logedPass){

            userProfile.edit().putString(LOGED_pass, logedPass).commit();
        }

    }

    public static class PreferenceUserSettings {

        public final String USER_SETTINGS = "UserSettings";

        private static final String CURRENT_SITE_ID = "CurrentSiteID";
        private static final String CURRENT_SITE_NAME = "CurrentSiteName";
        private static final String UNIT_DISTANCE = "UnitDistance";
        private static final String UNIT_TEMPERATURE = "UnitTemperature";
        private static final String SCREEN_AUTO_LOCK = "ScreenAutoLock";
        private static final String AVATR_INDEX = "AvatarIndex";
        private static final String EXPERIENCE = "Experience";

        /* SETTING KEYS*/
        public static final String OFFERS = "offers";
        public static final String DELIVERED = "delivered";
        public static final String MESSAGES = "messages";
        public static final String TIMEOUTS = "timeouts";
        public static final String PACKAGES = "packages";
        public static final String MATCHES = "matches";

        public static final String DATE_FORMAT = "dateFormat";
        public static final String TIME_FORMAT = "timeFormat";
        public static final String NOTIFY_RELEVANT_KEY = "notifyRelevantpick";
        private static final String CURRENCY_COUNTRY = "currencyCountry";
        private static final String CURRENCY_CODE = "currencyCode";

        private SharedPreferences userSettings;
        public static PreferenceUserSettings userSettingInstance;

        private PreferenceUserSettings() {
            userSettings = context.getSharedPreferences(USER_SETTINGS, Context.MODE_PRIVATE);
        }

        public static PreferenceUserSettings getUserSettingsInstance() {
            if (userSettingInstance == null) {
                userSettingInstance = new PreferenceUserSettings();
            }
            return userSettingInstance;
        }

        public void removeInstance() {
            userSettings.edit().clear().commit();
            userSettingInstance = null;
        }


        /************
         * SETTING
         *********************/

        /* timer */

        public static final String TIMER_STATE = "timerState";

        public void setTimerState(int timerState) {
            userSettings.edit().putInt(TIMER_STATE, timerState).commit();
        }

        public int getTimerState(){
            return userSettings.getInt(TIMER_STATE, 0);
        }

        public static final String TIMER_DIFFERENCE = "timerDifference";

        public void setTimerDifference(long timerDifference) {
            userSettings.edit().putLong(TIMER_DIFFERENCE, timerDifference).commit();
        }

        public long getTimerDifference(){
            return userSettings.getLong(TIMER_DIFFERENCE, 0);
        }

        public static final String TIMER_START = "timerStart";

        public void setTimerStart(long timerStart) {
            userSettings.edit().putLong(TIMER_START, timerStart).commit();
        }

        public long getTimerStart(){
            return userSettings.getLong(TIMER_START, 0);
        }

        public static final String TIME_WHEN_STOPPED = "timeWhenStopped";

        public void setTimeWhenStopped(long timeWhenStopped) {
            userSettings.edit().putLong(TIME_WHEN_STOPPED, timeWhenStopped).commit();
        }

        public long getTimeWhenStopped(){
            return userSettings.getLong(TIME_WHEN_STOPPED, 0);
        }

        /*  --  */


        public void setNotifyRelevantpick(boolean notifyRelevantpick) {
            userSettings.edit().putBoolean(NOTIFY_RELEVANT_KEY, notifyRelevantpick).commit();
        }

        public boolean getNotifyRelevantpick() {
            return userSettings.getBoolean(NOTIFY_RELEVANT_KEY, false);
        }

        public int isNotifyOffers() {
            return userSettings.getInt(OFFERS, 1);
        }

        public void setNotifyOffers(int offers) {
            userSettings.edit().putInt(OFFERS, offers).commit();
        }

        public int isNotifyDelivered() {
            return userSettings.getInt(DELIVERED, 1);
        }

        public void setNotityDelivered(int delivered) {
            userSettings.edit().putInt(DELIVERED, delivered).commit();
        }

        public int isNotifyNewMessages() {
            return userSettings.getInt(MESSAGES, 1);
        }

        public void setNotifyNewMessages(int messages) {
            userSettings.edit().putInt(MESSAGES, messages).commit();
        }

        public int isNotifyNewPackages() {
            return userSettings.getInt(PACKAGES, 1);
        }

        public void setNotifyNewPackages(int packages) {
            userSettings.edit().putInt(PACKAGES, packages).commit();
        }

        public int isNotifyMachesBids() {
            return userSettings.getInt(MATCHES, 1);
        }

        public void setNotifyMachesBids(int matches) {
            userSettings.edit().putInt(MATCHES, matches).commit();
        }

        public int isNotifyEventsBeforExpiration() {
            return userSettings.getInt(TIMEOUTS, 1);
        }

        public void setNotifyEventsBeforExpiration(int timeouts) {
            userSettings.edit().putInt(TIMEOUTS, timeouts).commit();
        }

        public int getDateFormat() {
            return userSettings.getInt(DATE_FORMAT, 0);
        }

        public void setDateFormat(int dateFormat) {
            userSettings.edit().putInt(DATE_FORMAT, dateFormat).commit();
        }

        public int getTimeFormat() {
            return userSettings.getInt(TIME_FORMAT, 0);
        }

        public void setTimeFormat(int timeFormat) {
            userSettings.edit().putInt(TIME_FORMAT, timeFormat).commit();
        }

        public void setCurrentSiteID(String siteID) {
            userSettings.edit().putString(CURRENT_SITE_ID, siteID).commit();
        }

        public void setCurrentSiteName(String siteName) {
            userSettings.edit().putString(CURRENT_SITE_NAME, siteName).commit();
        }

        public void setDistanceUnit(boolean isMile) {
            userSettings.edit().putBoolean(UNIT_DISTANCE, isMile).commit();
        }

        public void setTemperatureUnit(boolean isFahrenheit) {
            userSettings.edit().putBoolean(UNIT_TEMPERATURE, isFahrenheit).commit();
        }

        public void setScreenAutoLock(boolean isScreenAutoLock) {
            userSettings.edit().putBoolean(SCREEN_AUTO_LOCK, isScreenAutoLock).commit();
        }

        public void setAvatarIndex(int index) {
            userSettings.edit().putInt(AVATR_INDEX, index).commit();
        }

        public void setExperience(int experience) {
            userSettings.edit().putInt(EXPERIENCE, experience).commit();
        }


        // GET
        public String getCurrentSiteID() {
            return userSettings.getString(CURRENT_SITE_ID, "");
        }

        public String getCurrentSiteName() {
            return userSettings.getString(CURRENT_SITE_NAME, "None");
        }

        public boolean getDistanceUnit() {
            return userSettings.getBoolean(UNIT_DISTANCE, false);
        }

        public String getDistanceUnitName() {
            if (getDistanceUnit()) {
                return "Mile";
            }
            return "Km";
        }

        public double getDistanceUnitFactor() {
            if (getDistanceUnit()) {
                return 1609.34;

            }
            return 1000;
        }

        public boolean getTemperatureUnit() {
            return userSettings.getBoolean(UNIT_TEMPERATURE, false);
        }

        public boolean getScreenAutoLock() {
            return userSettings.getBoolean(SCREEN_AUTO_LOCK, true);
        }

        public int getAvatarIndex() {
            return userSettings.getInt(AVATR_INDEX, 0);
        }

        public int getExperience() {
            return userSettings.getInt(EXPERIENCE, 1);
        }


        public String getCurrencyCountry() {
            return userSettings.getString(CURRENCY_COUNTRY, "");
        }

        public void setCurrencyCountry(String currencyCountry) {
            userSettings.edit().putString(CURRENCY_COUNTRY, currencyCountry).commit();
        }

        public String getCurrencyCode() {
            return userSettings.getString(CURRENCY_CODE, "");
        }

        public void setCurrencyCode(String currencyCode) {
            userSettings.edit().putString(CURRENCY_CODE, currencyCode).commit();
        }


        public void removeAllSettings() {
            userSettings.edit().clear().commit();
        }
    }


}
